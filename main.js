// Assignment 4

function charReverseHandler () {

}
charReverseHandler()

function createListHandler () {
    firstArray = [1,2,3,4,5,6,'apple']
    secondArray = [1,2,3,4,5,6,7,'apple']
    sameArray = []
    for(let item of firstArray){
        for(let secondItem of secondArray){
            if(item == secondItem){
                sameArray.push(item)
            }
        }
    }
    console.log(sameArray)
}
createListHandler()
function findDublicatedHandler () {

}

function union(arra1, arra2) {
  
    if ((arra1 == null) || (arra2==null)) 
      return void 0;
    
    var obj = {};
   
    for (var i = arra1.length-1; i >= 0; -- i)
       obj[arra1[i]] = arra1[i];
   
    for (var i = arra2.length-1; i >= 0; -- i)
       obj[arra2[i]] = arra2[i];
   
    var res = [];
   
    for (var n in obj)
    {
    
      if (obj.hasOwnProperty(n)) 
        res.push(obj[n]);
    }
   
    return res;
  }
  console.log(union([1, 2, 3], [100, 2, 1, 10]));
  
function unionHandler(){

let elements = [1,2,3,false,undefined,'',NaN]
let index = 1
for(let i of elements){
    if(i == '' ){
        elements.pop()
}else if(i == false){
    elements.pop()
}else if(i == NaN){
    elements.pop()
}else if(i == undefined){
    elements.pop()
}

}
console.log(elements)
}
unionHandler()

function reverseStringHandler () {
    // your code here
    const sor = str => str.split('').sort((a, b) => a.localeCompare(b)).join('');
    console.log(sor('Hello world'))
}
reverseStringHandler()

// to do list
const form = document.querySelector('form')
const text = document.querySelector('#text')
const table = document.querySelector('table')
let todos = []

function updateHtml(){
    table.innerHTML = ''
    for(let todo of todos){
        const row = document.createElement('tr')
        const firstColumn = document.createElement('td')
        const input = document.createElement('input') 
        input.setAttribute("type","checkbox")
        input.checked = todo.isCompleted 
        input.addEventListener('change',function(){
            todo.isCompleted = !todo.isCompleted
            updateHtml()
        })
        firstColumn.appendChild(input)

        const secondColumn = document.createElement('td')
        secondColumn.textContent = todo.value
        if(todo.isCompleted){
        secondColumn.style.textDecoration = 'line-through'
        }

        const thirdColumn = document.createElement('td')
        thirdColumn.textContent = "Delete"
        thirdColumn.addEventListener('click',function(){
            const unfilteredTodos=todos;

            todos=unfilteredTodos.filter(element=>{
                console.log(element.id, todo.id)
                return todo.id !== element.id})
            console.log(todos)
            updateHtml()
        })
        
        row.appendChild(firstColumn)
        row.appendChild(secondColumn)
        row.appendChild(thirdColumn)

        table.appendChild(row)



    }
}
 

function handleAdd(event){
    event.preventDefault();
    
    todos.push({value: text.value,isCompleted: false, id: Date.now()})
    event.target.reset()
    updateHtml()
}

form.addEventListener('submit',handleAdd)
